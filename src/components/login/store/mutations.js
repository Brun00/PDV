import * as types from './types'

export default {
  [types.setUsers] (state, users) {
    state.users = users
  },
  [types.deleteUser] (state, id) {
    const filtered = (x) => state.users.filter((y) => y.id !== x)
    state.users = filtered(id)
    localStorage.setItem('users', JSON.stringify(state.users))
  }
}
