import * as types from './types'

export const setUsers = (context, payload) => {
  context.commit(types.setUsers, payload)
}
export const deleteUser = (context, payload) => {
  context.commit(types.deleteUser, payload)
}
